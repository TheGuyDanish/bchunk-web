# bchunk-web

This is a tiny Flask project made to give a frontend for bchunk for my friends who do not have a Linux box available to use it, and don't want to download a separate application to do it.

It can be started by running: `FLASK_APP=bchunk.py flask run` on any machine with bchunk enabled. Not that it does not test for the existence of bchunk, so ensure you install it beforehand.

You can also pull it from the Docker Hub and use it from there.
```sh
KEY=$(pwgen 32 1) # Or whatever key you'd like.
docker pull theguydanish/bchunk-web
docker run -d -e SECRET_KEY=$KEY -p 8080:5000 theguydanish/bchunk-web
```