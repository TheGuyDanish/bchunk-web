"""
bchunk-web is a simple Flask script to act as an interface to bchunk.

bchunk provides CD image format conversion between BIN/CUE to ISO/CDR by Heikki Hannikainen.
http://he.fi/bchunk/

bchunk-web aims to make it simple to run it without a Linux device present,
Windows apps do exist but none of them are as quick and easy to use as bchunk
(assuming your internet connection is decent)
"""

import atexit
import os
import time
import subprocess

from random import randrange
from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, render_template, request, send_file, abort, flash, redirect, url_for
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileRequired, FileField

APP = Flask(__name__)

# A secret key may seem overkill for this, but wtforms requires it for CSRF tokens.
APP.config['SECRET_KEY'] = os.environ.get('SECRET_KEY') or "youwillneverguessthis"
APP.config['VERSION'] = "INTERNAL_DEV_VERSION"
UPLOAD_FOLDER = 'tmp'
DOWNLOAD_FOLDER = 'download'

def cleanup_downloads():
    """ Function to remove older downloads to be run on a schedule. """
    print('[CRON] Cleaning up expired files')
    path = DOWNLOAD_FOLDER
    now = time.time()
    for file in os.listdir(path):
        file = os.path.join(path, file)
        if os.stat(file).st_mtime < now - 7 * 86400:
            if os.path.isfile(file):
                os.remove(file)
                print(f'[CRON] Removed {file}')

# Run the above function on a timer every hour.
SCHEDULER = BackgroundScheduler()
SCHEDULER.add_job(func=cleanup_downloads, trigger='interval', seconds=3600)
SCHEDULER.start()
print('[CRON] Started cron scheduler')

# pylint says this lambda isn't necessary. pylint is very wrong.
# pylint: disable=unnecessary-lambda
atexit.register(lambda: SCHEDULER.shutdown())

class UploadForm(FlaskForm):
    """ A simple form that accepts the uploaded files, and only with the required types. """
    bin_file = FileField('bin', validators=[FileRequired(),
                                            FileAllowed(['bin'], 'BIN files only!')])
    cue_file = FileField('cue', validators=[FileRequired(),
                                            FileAllowed(['cue'], 'CUE files only!')])

@APP.context_processor
def inject_version():
    """ Dirty little function to put our version into a variable in every route. """
    return dict(version=APP.config['VERSION'])


@APP.route('/', methods=['GET', 'POST'])
def index():
    """
    This function serves as both:
    The index, the ability to upload, and the post-run message.

    First, it greets the user with an upload form, where they can provide their files.
    Then it runs bchunk with the uploaded files, given a random name, for safety.
    It waits for bchunk to terminate before continuing, then checks if the final file exists.
    If it does, it provides the download link to the user, if not, it flashes an error.
    """
    form = UploadForm()

    if form.validate_on_submit():
        name = randrange(1, 2147483647)
        bin_file = request.files['bin_file']
        cue_file = request.files['cue_file']

        # Save the files under new names to avoid any funky command line escaping.
        if bin_file:
            bin_filename = f'{name}.bin'
            bin_file.save(os.path.join(UPLOAD_FOLDER, bin_filename))
        if cue_file:
            cue_filename = f'{name}.cue'
            cue_file.save(os.path.join(UPLOAD_FOLDER, cue_filename))

        process = subprocess.Popen(["bchunk",
                                    f'{UPLOAD_FOLDER}/{bin_filename}',
                                    f'{UPLOAD_FOLDER}/{cue_filename}',
                                    f'{DOWNLOAD_FOLDER}/{name}.'])
        process.wait()

        # Remove temporary files once work is finished.
        os.remove(f'{UPLOAD_FOLDER}/{bin_filename}')
        os.remove(f'{UPLOAD_FOLDER}/{cue_filename}')
        final_path = f'{DOWNLOAD_FOLDER}/{name}.01.iso'

        if os.path.isfile(final_path):
            flash('<h4 class="alert-heading">Your file was converted!</h4><hr />'\
                  f'<p>You can download it <a class="alert-link" href="{final_path}">here</a>'\
                  '<br /><small>This link will expire in a week.</p>', 'success')
            return render_template('index.html', form=form)
        else:
            flash('<h4 class="alert-heading">Your file could not be converted.</h4>'\
                  '<hr />Please check that your files are valid or attempt again.', 'danger')
            return redirect(url_for('index'))

    return render_template('index.html', form=form)

@APP.route('/download/<req_file>')
def download(req_file):
    """ As we cannot directly download a file from a Flask subdirectory
        Do a hacky little function instead. """
    file_name = f'{DOWNLOAD_FOLDER}/{req_file}'
    if os.path.isfile(file_name):
        return send_file(file_name)
    else:
        abort(404)
