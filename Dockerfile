FROM python:3.7-stretch

RUN apt-get update 
RUN apt-get install -y bchunk

RUN useradd -m -s /bin/bash bchunk

WORKDIR /home/bchunk

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY templates templates
COPY bchunk.py boot.sh ./
RUN chmod +x boot.sh
RUN mkdir download
RUN mkdir tmp

ENV FLASK_APP bchunk.py

RUN chown -R bchunk:bchunk ./
USER bchunk

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]